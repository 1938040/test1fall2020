package tests;
import automobiles.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

//Jamin huang 1938040 2020-09-29
class CarTests {
	
	@Test
	void speedTest() {
		Car car1=new Car(200);
		assertEquals(200,car1.getSpeed());
		
		try {
			Car car2=new Car(-10);
			assertEquals(1,car2.getSpeed());
			fail("supposed to throw exception");
		}
			catch (IllegalArgumentException e) {
				//catch the illegal argument
		}
		catch (Exception b) {
			fail("failed to catch IllegalArgument as supposed to, caught others");
		}
		Car car3=new Car(0);
		assertEquals(0,car3.getSpeed());
	}
	@Test
	void StartingLocationTest() {
		Car car1=new Car(200);
		assertEquals(50,car1.getLocation());
	}
	@Test
	void LocationTest() {
		Car car1=new Car(200);
		car1.moveRight();
		assertEquals(100,car1.getLocation());
		
		Car car2=new Car(10);
		car2.moveRight();
		assertEquals(60,car2.getLocation());
		
		car1.moveLeft();
		assertEquals(0,car1.getLocation());
		
		car2.moveLeft();
		assertEquals(50,car2.getLocation());
		
		Car car3=new Car(50);
		car3.moveLeft();
		assertEquals(0,car3.getLocation());
		
		Car car4=new Car(7);
		car4.moveLeft();
		assertEquals(43,car4.getLocation());
		
		car3.moveRight();
		assertEquals(50,car3.getLocation());
		
		car4.moveRight();
		assertEquals(50,car4.getLocation());
	}
	@Test
	void accelerateTest() {
		Car car1=new Car(200);
		car1.accelerate();
		assertEquals(201,car1.getSpeed());
		
		Car car2=new Car(5);
		car2.accelerate();
		car2.accelerate();
		assertEquals(7,car2.getSpeed());
	}
	@Test
	void stopTest() {
		Car car1=new Car(200);
		car1.stop();
		assertEquals(0,car1.getSpeed());
		
		Car car2=new Car(0);
		car2.stop();
		car2.stop();
		assertEquals(0,car2.getSpeed());
	}
	
}
