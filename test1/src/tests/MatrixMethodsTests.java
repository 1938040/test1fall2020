package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.*;

//Jamin Huang 1938040 2020-09-29
class MatrixMethodsTests {
	
	
	@Test
	void MatrixTest() {
		int[][] arr1={{1,2,3},{4,5,6}};
		int[][] arrNew1=MatrixMethod.duplicate(arr1);
		
		int[][] arrSol1={{1,1,2,2,3,3},{4,4,5,5,6,6}};
		assertArrayEquals(arrSol1,arrNew1);
		
		
		int[][] arr2={{4}};
		int[][] arrNew2=MatrixMethod.duplicate(arr2);
		
		int[][] arrSol2={{4,4}};
		assertArrayEquals(arrSol2,arrNew2);
	}
	@Test
	void MatrixTestNullValues() {
		int[][] arr1={{},{}};
		int[][] arrNew1=MatrixMethod.duplicate(arr1);
		
		int[][] arrSol1={{},{}};
		assertArrayEquals(arrSol1,arrNew1);
	}
}