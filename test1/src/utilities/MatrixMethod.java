package utilities;
//Jamin Huang 1938040 2020-09-29
public class MatrixMethod {
	public static int[][] duplicate(int [][] arr){
		//Assuming we should multiply the number of indexes for the second index [2][3]=>[2][6] as displayed in the test example
		
		int count=arr.length;
		int count2=arr[0].length;
		
		int [][] arrNew=new int[count][2*count2];
		
		for(int i=0;i<count;i++) {
			for(int j=0;j<count2;j++) {
				arrNew[i][j*2]=arr[i][j];
				arrNew[i][j*2+1]=arr[i][j];
			}
		}
		return arrNew;
	}
}
